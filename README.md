# ssg-subfactions

A Starsector mod which splits the Independents and Pirates factions into small subfactions. The indepedent subfactions are "procedurally-generated" in that their names, logos/crests, illegal goods, Nexerelin morality, and arsenal are semi-randomly generated. The illegal goods and arsenal take neighboring factions into account (e.g., an independent faction nearby a few Hegemony systems may have AI cores set to illegal since various Hegemony individuals may have influence within the subfaction). This mod does not change procedural generation and should be able to handle mods adding in new Independent or Pirates systems in either Nex random core worlds or the vanilla Sector.

Currently, pirate subfactions are partially implemented but are disabled by default. To enable, go to the ssg_config.json and change the appropriate line to ``true``. Note that there are not enough pirate subfactions to fully replace all default Pirate worlds in the vanilla Sector.

## Requirements

Nexerelin: https://bitbucket.org/Histidine/exerelin/downloads/

## Future Work

This is a beta release and is largely incomplete. Future additions are likely to include:

* pirate subfactions vying for control of the Sector's black markets

* further independent subfaction interactions, such as expeditions beyond the core worlds

* the ability to pit a pirate subfaction against another faction

* general expansion to the game's black market system

## Notes

The flags of the independent subfactions are taken from Japanese prefectures. This was done because 1) I am not artistically gifted 2) they are very good looking and I feel fit into the game artstyle well. If you can make nice logos and crests that fit into the vanilla game style then I'd be glad to add them in.

## Thanks to

* Alex and team for making Starsector such a great and mod-friendly game

* Zaphide and Histidine for (N)exerelin

* Feedback from the /ssg/ and the Starsector community
