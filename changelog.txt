0.2.1 (beta) 21/9/2019
>new industries will be randomly added to subfaction markets in non-random
core worlds
	>each building has a 75% chance to appear in a qualifying market
>added optional limits to the number of independent and pirate subfaction that
can spawn at sector gen in the ssg_config.json file
>fixed a bug where a subfaction in Prism wouldn't be added to the independent
subfaction alliance

0.2 (beta) 21/9/2019
>added randomized starting relations for indepedent/pirate subfactions
>added chance for independent faction to be friendly with a pirate subfaction
>independent/pirate subfactions now have alternating stripes on the ring
around their fleet on the campaign map. independent subfactions will have gray
stripes and pirate subfactions will have red stripes
>added small industry structures to independent and pirate worlds (for random
core worlds only) during proc
gen to help out with low ship numbers and quality due to the independent and
pirate subfactions being split up
	>Salvage Operations: hull production
	>Ship Restoration Drydocks: 15% bonus to fleet quality (additional
	+25% if the subfaction does not produce hulls to counteract
	cross-faction import penalty)
	>Luddic Path markets may also spawn these structures on random core
	worlds proc gen
>added a missing file required for version checker
>pirate subfactions no longer make pirate bases
>the boolean to toggle subfactions in the faction intel page now reads every
time it loads, letting users change the flag and have the change take effect
without having to start a new game

0.1.a (beta) 15/9/2019
>recompiled jar to prevent a crash

0.1.0 (beta) 15/9/2019
>initial release
