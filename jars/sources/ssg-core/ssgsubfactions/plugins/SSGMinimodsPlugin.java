package ssgsubfactions.plugins;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Industries;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;
import com.fs.starfarer.api.util.WeightedRandomPicker;

import exerelin.campaign.AllianceManager;
import exerelin.campaign.SectorManager;
import exerelin.campaign.alliances.Alliance;
import exerelin.utilities.ExerelinConfig;
import exerelin.utilities.ExerelinUtilsFaction;
import exerelin.utilities.ExerelinUtilsMarket;
import exerelin.utilities.StringHelper;
import ssgsubfactions.utilities.SSGConfig;
import ssgsubfactions.utilities.SSGUtilsMinorFactions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import org.apache.log4j.Logger;

public class SSGMinimodsPlugin extends BaseModPlugin
{
	public static final boolean HAVE_VERSION_CHECKER = Global.getSettings().getModManager().isModEnabled("lw_version_checker");
	
	public static Logger log = Global.getLogger(SSGMinimodsPlugin.class);
	
	public static final float INDEPENDENT_FRIENDLY_TO_PIRATES_CHANCE = 0.1f;
	
	public static final float NEW_INDUSTRY_ADDITION_CHANCE = 0.75f;
	
	@Override
    public void onApplicationLoad() throws Exception
    {
        if (!Global.getSettings().getModManager().isModEnabled("nexerelin")) {
            throw new RuntimeException(StringHelper.getString("ssgminimods_misc", "errorNoNex"));
        }
    }
	
	@Override
    public void onNewGameAfterEconomyLoad()
    {
		boolean haveNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
		log.info("SSG Mods loaded ");
		if (haveNexerelin)
		{
			breakupFactions();
			
			if (SectorManager.getCorvusMode())
			{ // add in new industries for Corvus mode - only handles indie/pirate markets
				// TODO probably a nicer implementation (i.e., hand made implementation)
				log.info("Adding ssg-subfaction industries to non-random core worlds.");
				for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy())
				{
					if (market.hasIndustry(Industries.HEAVYINDUSTRY) || market.hasIndustry(Industries.ORBITALWORKS)) continue;
					if (SSGUtilsMinorFactions.isMinorFaction(market.getFaction()) && StarSystemGenerator.random.nextFloat() < NEW_INDUSTRY_ADDITION_CHANCE)
					{
						market.addIndustry("salvageoperations");
					}
					if (SSGUtilsMinorFactions.isMinorFaction(market.getFaction()) && StarSystemGenerator.random.nextFloat() < NEW_INDUSTRY_ADDITION_CHANCE)
					{
						market.addIndustry("shiprestorationdrydock");
					}
				}
			}
			
			List<FactionAPI> independents = new ArrayList<FactionAPI>();
			List<FactionAPI> pirates = new ArrayList<FactionAPI>();
			
			for (FactionAPI f : Global.getSector().getAllFactions())
			{
				if (!ExerelinUtilsFaction.hasAnyMarkets(f.getId())) continue;
				
				if (SSGUtilsMinorFactions.isMinorIndependent(f.getId())) independents.add(f);
				else if (SSGUtilsMinorFactions.isMinorPirate(f.getId())) pirates.add(f);
			}
			
			if (pirates.size() == 0 && !SSGConfig.enableMinorPirates) // use vanilla pirates instead
			{
				for (FactionAPI f : Global.getSector().getAllFactions())
				{
					if (f.getId().equals(Factions.PIRATES)) pirates.add(f); break;
				}
			}
			
			setRelations(independents, pirates);
		}
    }
	
	@Override
    public void onGameLoad(boolean newGame) {
        if (!newGame)
        {
        	for (FactionAPI f : Global.getSector().getAllFactions())
        	{
        		if (ExerelinUtilsFaction.hasAnyMarkets(f.getId()) && SSGUtilsMinorFactions.isMinorIndependent(f.getId()))
        		{
        			SSGConfig.usedFlags.add(f.getFactionCrestOverride().substring(0, f.getFactionCrestOverride().length() - 10));
        			ExerelinConfig.getExerelinFactionConfig(f.getId()).playableFaction = true;
        			f.setShowInIntelTab(SSGConfig.showMinorIndependentsInIntel);
        		}
        		else if (ExerelinUtilsFaction.hasAnyMarkets(f.getId()) && SSGUtilsMinorFactions.isMinorPirate(f.getId()))
				{
        			ExerelinConfig.getExerelinFactionConfig(f.getId()).playableFaction = true;
        			f.setShowInIntelTab(SSGConfig.showMinorPiratesInIntel);
				}
        	}
        }
    }
	
	/**
	 * Break up independent and pirate factions, including generating the starting independent alliance
	 */
	protected void breakupFactions()
	{
		log.info("Looping over " + Global.getSector().getEconomy().getMarketsCopy().size() + " markets");
	    
		SSGConfig.usedFlags.clear();
		
		FactionAPI faction1 = null; // for starting the alliance
		FactionAPI faction2 = null;
		Alliance independentAlliance = null;
		
		int numIndependents = 0;
		int numPirates = 0;
		
		List<MarketAPI> marketsShuffled = Global.getSector().getEconomy().getMarketsCopy();
		Collections.shuffle(marketsShuffled, StarSystemGenerator.random);
		
		for (MarketAPI market : marketsShuffled)
		{
			if (market.getFactionId().equals(Factions.INDEPENDENT) && !(ExerelinUtilsMarket.NO_INVADE_MARKETS).contains(market.getId())) // don't pick prism
			{
				if (SSGConfig.limitNumberIndependents && numIndependents >= SSGConfig.maxNumberOfStartingIndependents) continue;
				log.info("Found independent market " + market.getName());
				
				FactionAPI newFaction = SSGUtilsMinorFactions.createNewMinorFaction(market, false);
				
				if (newFaction == null)
				{
					log.info(market.getName() + " rolled a null faction.");
					continue;
				}
				
				numIndependents++;
				if (numIndependents == SSGConfig.maxNumberOfStartingIndependents)
					log.info("Reached number of starting independent subfactions.");
				
				if (market.getStarSystem() != null)
				{
					// generate nearby systems list
					List<MarketAPI> nearbyMarkets = new ArrayList<MarketAPI>();
					for (SectorEntityToken sET : market.getStarSystem().getAllEntities())
					{
						MarketAPI mk = sET.getMarket();
						if (mk != null)
						{
							nearbyMarkets.add(mk);
						}
					}
					
					SSGUtilsMinorFactions.exchangeTech(newFaction, nearbyMarkets, true, true, true);
					SSGUtilsMinorFactions.illegalCommodityRandomization(newFaction, nearbyMarkets);
					
				}
				
				if (faction1 == null) faction1 = newFaction;
				else if (faction2 == null) {faction2 = newFaction; independentAlliance = AllianceManager.createAlliance(faction1.getId(), faction2.getId(), Alliance.Alignment.DIPLOMATIC); }
				else 
				{
					independentAlliance.addMember(newFaction.getId());
				}
			}
			else if (market.getFactionId().equals(Factions.PIRATES) && ExerelinUtilsMarket.canBeInvaded(market, false) && SSGConfig.enableMinorPirates)
			{
				if (SSGConfig.limitNumberPirates && numPirates >= SSGConfig.maxNumberOfStartingPirates) continue;
				log.info("Found pirate market " + market.getName());
				
				String oldOwner = market.getFactionId();
				FactionAPI newFaction = SSGUtilsMinorFactions.createNewMinorFaction(market, true);
				
				if (newFaction != null)
				{
					SectorManager.updateSubmarkets(market, oldOwner, newFaction.getId());
					
					numPirates++;
					if (numPirates == SSGConfig.maxNumberOfStartingPirates)
						log.info("Reached number of starting pirate subfactions.");
					
					/*for (SubmarketAPI s : market.getSubmarketsCopy())
					{
						if (s.getPlugin().isBlackMarket()) s.setFaction(newFaction);
					}*/ // TODO re-enable when black market mechanics are revamped
					
					//SectorManager.transferMarket(market, newFaction, market.getFaction(), false, false, null, 0);
					
					//SSGUtilsMinorFactions.renameFaction(newFaction, true);
				}
			}
		}
	}
	
	protected void setRelations(List<FactionAPI> independents, List<FactionAPI> pirates)
	{
		WeightedRandomPicker<RepLevel> relations = new WeightedRandomPicker<>(StarSystemGenerator.random);
		
		for (FactionAPI f : independents)
		{
			ListIterator<FactionAPI> it = independents.listIterator(independents.indexOf(f));
			while (it.hasNext())
			{
				FactionAPI g = it.next();
				if (f.equals(g)) continue; // just in case
				
				StarSystemAPI system1 = ExerelinUtilsFaction.getFactionMarkets(f.getId()).get(0).getStarSystem();
				StarSystemAPI system2 = ExerelinUtilsFaction.getFactionMarkets(g.getId()).get(0).getStarSystem();
				
				boolean inSameSystem = false;
				
				if (system1 != null && system2 != null)
				{
					inSameSystem = system1.equals(system2);
				}
				
				float weight = 1.0f;
				relations.add(RepLevel.COOPERATIVE, weight);
				relations.add(RepLevel.FRIENDLY, weight);
				
				weight = 2.0f;
				if (inSameSystem)
					weight += 1.0f; // same system so have likely done successful business
				relations.add(RepLevel.WELCOMING, weight);
				relations.add(RepLevel.FAVORABLE, weight);
				
				weight = 5.0f;
				relations.add(RepLevel.NEUTRAL, weight);
				
				weight = 2.0f;
				if (inSameSystem)
					weight += 0.5f; // same system so may have gotten into some disagreements
				relations.add(RepLevel.SUSPICIOUS, weight);
				relations.add(RepLevel.INHOSPITABLE, weight);
				
				weight = 0.5f;
				relations.add(RepLevel.HOSTILE, weight);
				relations.add(RepLevel.VENGEFUL, weight);
				
				f.setRelationship(g.getId(), relations.pick());
				relations.clear();
			}
		}
		
		for (FactionAPI f : pirates)
		{
			ListIterator<FactionAPI> it = pirates.listIterator(pirates.indexOf(f));
			while (it.hasNext())
			{
				FactionAPI g = it.next();
				if (f.equals(g)) continue; // just in case
				
				boolean inSameSystem = ExerelinUtilsFaction.getFactionMarkets(f.getId()).get(0).getStarSystem().equals(ExerelinUtilsFaction.getFactionMarkets(g.getId()).get(0).getStarSystem());
				
				float weight = 1.0f;
				relations.add(RepLevel.COOPERATIVE, weight);
				relations.add(RepLevel.FRIENDLY, weight);
				
				weight = 2.0f;
				if (inSameSystem)
					weight += 0.5f; // same system so have likely done successful business
				relations.add(RepLevel.WELCOMING, weight);
				relations.add(RepLevel.FAVORABLE, weight);
				
				weight = 5.0f;
				relations.add(RepLevel.NEUTRAL, weight);
				
				weight = 2.0f;
				if (inSameSystem)
					weight += 1.5f; // same system so may have gotten into some disagreements
				relations.add(RepLevel.SUSPICIOUS, weight);
				relations.add(RepLevel.INHOSPITABLE, weight);
				
				weight = 0.5f;
				relations.add(RepLevel.HOSTILE, weight);
				relations.add(RepLevel.VENGEFUL, weight);
				
				f.setRelationship(g.getId(), relations.pick());
				relations.clear();
			}
		}
		
		// add a chance for independents to be friendly to pirates
		for (FactionAPI f : independents)
		{
			for (FactionAPI g : pirates)
			{
				if (StarSystemGenerator.random.nextFloat() < INDEPENDENT_FRIENDLY_TO_PIRATES_CHANCE)
				{
					relations.add(RepLevel.COOPERATIVE, 0.6f);
					relations.add(RepLevel.FRIENDLY, 1.0f);
					relations.add(RepLevel.WELCOMING, 0.5f);
					
					f.setRelationship(g.getId(), relations.pick());
					
					log.info("Relations between independent " + f.getDisplayName() + " and pirate " + g.getDisplayName() + " have been set to " + f.getRelationship(g.getId()));
					
					relations.clear();
				}
			}
		}
	}
}
