package ssgsubfactions.console.commands;

import static exerelin.console.commands.SpawnInvasionFleet.getMarket;

import org.lazywizard.console.BaseCommand;
import org.lazywizard.console.CommonStrings;
import org.lazywizard.console.Console;

import com.fs.starfarer.api.campaign.econ.MarketAPI;

import ssgsubfactions.campaign.fleets.RefugeeFleetManagerV2;

public class SpawnRefugeeFleet implements BaseCommand
{

	@Override
	public CommandResult runCommand(String args, CommandContext context) {
		if (!context.isInCampaign())
		{
			Console.showMessage(CommonStrings.ERROR_CAMPAIGN_ONLY);
			return CommandResult.WRONG_CONTEXT;
		}
		
		String[] tmp = args.split(" ");
		if (tmp.length < 1)
		{
			return CommandResult.BAD_SYNTAX;
		}
		
		MarketAPI source = getMarket(tmp[0]);
		MarketAPI target = null;
		if (tmp.length == 2)
		{
			target = getMarket(tmp[1]);
		}
		
		if (source == null)
		{
			Console.showMessage("Invalid source market");
			return CommandResult.ERROR;
		}
		if (target == null && tmp.length == 2)
		{
			Console.showMessage("Invalid target market");
			return CommandResult.ERROR;
		}
		
		RefugeeFleetManagerV2 rfm = new RefugeeFleetManagerV2();
		if (tmp.length == 2)
		{
			//rfm.spawnRefugeeFleet(source, target);
			rfm.spawnRefugeeFleet(source);
		}
		else
		{
			rfm.spawnRefugeeFleet(source);
		}
		return CommandResult.SUCCESS;
	}

}
