package ssgsubfactions.console.commands;

import java.util.List;

import org.lazywizard.console.BaseCommand;
import org.lazywizard.console.CommonStrings;
import org.lazywizard.console.Console;
import org.lazywizard.console.BaseCommand.CommandResult;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;

import exerelin.utilities.ExerelinUtilsFaction;
import ssgsubfactions.utilities.SSGUtilsMinorFactions;

public class ListMinorFactions implements BaseCommand {

	@Override
	public CommandResult runCommand(String args, CommandContext context) {
		if (!context.isInCampaign())
		{
			Console.showMessage(CommonStrings.ERROR_CAMPAIGN_ONLY);
			return CommandResult.WRONG_CONTEXT;
		}
		
		Console.showMessage("Faction_id             current faction name             current faction flag");
		Console.showMessage("----------------------------------------------------------------------------");
		
		for (FactionAPI faction : Global.getSector().getAllFactions())
		{
			if (SSGUtilsMinorFactions.isMinorFaction(faction) && ExerelinUtilsFaction.hasAnyMarkets(faction.getId()))
			{
				String crestName = SSGUtilsMinorFactions.isMinorPirate(faction.getId()) ? "" : faction.getFactionCrestOverride().substring(0, faction.getFactionCrestOverride().length() - 10);
				Console.showMessage(faction.getId() + "  " + faction.getDisplayName() + "  " + crestName);
			}
		}
		
		Console.showMessage("\n\nSubmarket             market             current faction name");
		Console.showMessage("----------------------------------------------------------------------------");
		
		for (MarketAPI m : Global.getSector().getEconomy().getMarketsCopy())
		{
			for (SubmarketAPI s : m.getSubmarketsCopy())
			{
				if (SSGUtilsMinorFactions.isMinorPirate(s.getFaction().getId()))
				{
					Console.showMessage(s.getNameOneLine() + "  " + m.getName() + "  " + s.getFaction().getDisplayName());
				}
			}
		}
		
		return CommandResult.SUCCESS;
	}

}
