package ssgsubfactions.console.commands;

import org.lazywizard.console.BaseCommand;
import org.lazywizard.console.CommonStrings;
import org.lazywizard.console.Console;

import com.fs.starfarer.api.Global;

import ssgsubfactions.utilities.SSGUtilsMinorFactions;

public class RenameMinorFaction implements BaseCommand {

	@Override
	public CommandResult runCommand(String args, CommandContext context) {
		if (!context.isInCampaign())
		{
			Console.showMessage(CommonStrings.ERROR_CAMPAIGN_ONLY);
			return CommandResult.WRONG_CONTEXT;
		}
		
		String[] tmp = args.split(" ");
		if (tmp.length != 2)
		{
			return CommandResult.BAD_SYNTAX;
		}
		
		String factionId = tmp[0];
		String newName = tmp[1];
		
		if (SSGUtilsMinorFactions.isMinorFaction(factionId))
		{
			SSGUtilsMinorFactions.renameFaction(Global.getSector().getFaction(factionId), newName);
		}
		else
		{
			return CommandResult.ERROR;
		}
		
		return CommandResult.SUCCESS;
	}

}
