package ssgsubfactions.utilities;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;

import exerelin.campaign.SectorManager;
import exerelin.utilities.ExerelinConfig;
import exerelin.utilities.ExerelinFactionConfig.Morality;
import exerelin.utilities.ExerelinUtilsFaction;

public class SSGUtilsMinorFactions {
	
	public static Logger log = Global.getLogger(SSGUtilsMinorFactions.class);
	
	// change that a particular hull/weapon/fighter will be exchanged with the minor faction
	public static final float EXCHANGE_CHANCE = 0.005f;
	public static final int MAX_NUMBER_OF_EXCHANGE_PER_CATEGORY = 3;
	public static final float ILLEGAL_EXCHANGE_CHANCE = 0.1f;
	
	public static boolean isMinorFaction(FactionAPI faction)
	{
		return isMinorFaction(faction.getId());
	}
	
	public static boolean isMinorFaction(String factionID)
	{
		return (isMinorIndependent(factionID) || isMinorPirate(factionID));
	}
	
	public static boolean isMinorIndependent(String factionID)
	{
		if (Arrays.asList(SSGConfig.minorFactionList).contains(factionID))
			return true;
		return false;
	}
	
	public static boolean isMinorPirate(String factionID)
	{
		if (Arrays.asList(SSGConfig.minorPiratesList).contains(factionID))
			return true;
		return false;
	}
	
	/**
	 * If possible, find an empty minor faction and create as a new faction
	 * @return new (new meaning was not previously in play a moment ago) minor faction or null
	 */
	public static FactionAPI createNewMinorFaction(boolean isPirate)
	{
		return createNewMinorFaction(null, isPirate);
	}

	/**
	 * If possible, find an empty minor faction and create as a new faction
	 * @return new (new meaning was not previously in play a moment ago) minor faction or null
	 */
	public static FactionAPI createNewMinorFaction(MarketAPI market, boolean isPirate)
	{
		int numMinor = isPirate ? SSGConfig.minorPiratesList.length : SSGConfig.minorFactionList.length;
		
		Set<Integer> notTaken = new HashSet<Integer>();
		
		for (int i = 0; i < numMinor; i++)
		{
			if (isPirate && !minorFactionLives(SSGConfig.minorPiratesList[i]))
				notTaken.add(i);
			else if (!isPirate && !minorFactionLives(SSGConfig.minorFactionList[i]))
				notTaken.add(i);
		}
		
		if (notTaken.size() == 0) { log.info("All temp factions are taken for " + (isPirate ? "pirates." : "inpedendents.")); return null; } // all are taken
		
		int num = (int) (notTaken.toArray())[StarSystemGenerator.random.nextInt(notTaken.size())];
		
		FactionAPI newFaction = null;
		if (isPirate)
			newFaction = Global.getSector().getFaction(SSGConfig.minorPiratesList[num]);
		else
			newFaction = Global.getSector().getFaction(SSGConfig.minorFactionList[num]);
		log.info("Creating a new minor faction; default name: " + newFaction.getDisplayName());
		
		if (market != null)
		{
			SSGUtilsMinorFactions.renameFaction(newFaction, market, isPirate);
			//randomizeMorality(newFaction);
			log.info("Giving " + market.getName() + " to the new faction " + newFaction.getDisplayName() + ".");
			SectorManager.transferMarket(market, newFaction, market.getFaction(), false, false, null, 0);
			ExerelinConfig.getExerelinFactionConfig(newFaction.getId()).playableFaction = true;
			
			if (isPirate) newFaction.setShowInIntelTab(SSGConfig.showMinorPiratesInIntel);
			else newFaction.setShowInIntelTab(SSGConfig.showMinorIndependentsInIntel);
		}
			
		if (!isPirate) randomizeFlag(newFaction, isPirate);
		
		return newFaction;
	}
	
	public static boolean minorFactionLives(FactionAPI faction)
	{
		return minorFactionLives(faction.getId());
	}
	
	public static boolean minorFactionLives(String factionId)
	{
		return ExerelinUtilsFaction.hasAnyMarkets(factionId);
	}
	
	public static void renameFaction(FactionAPI faction, boolean isPirate)
	{
		renameFaction(faction, null, isPirate);
	}
	
	public static void renameFaction(FactionAPI faction, MarketAPI initialMarket, boolean isPirate)
	{
		int rand = StarSystemGenerator.random.nextInt();
		
		String[] suffixes;
		
		if (isPirate)
		{
			suffixes = new String[] {"Gang", "Group", "Syndicate", "Crew", "Cartel", "Cabal", "Clan"};
		}
		else
		{
			suffixes = new String[] {"Group", "Syndicate", "Republic", "Initiative", "Centre", "Association", "Consortium", "Programme", "Conglomerate", "Corp."};
		}
		
		switch (rand % 2) // TODO potentially randomize the suffixes according to faction "personalities"
		{
		case 0:
		default:
			String market_name = "ERROR IN NAMING";
			
			if (initialMarket == null)
			{
				for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy())
				{
					if (market.getFaction().equals(faction))
					{
						market_name = market.getName();
						break;
					}
				}
			}
			else
			{
				market_name = initialMarket.getName();
			}
			
			// fix faction name in case it contains junk from the market name
			market_name.replace("L4 ", "");
			market_name.replace("L5 ", "");
			
			String last_part = suffixes[StarSystemGenerator.random.nextInt(suffixes.length)];
			
			faction.setDisplayNameWithArticleOverride("The " + market_name + " " + last_part);
			renameFaction(faction, market_name + " " + last_part);
			faction.setDisplayIsOrAreOverride("is");
			
			break;
		}
	}
	
	// TODO probably isn't general enough
	public static void renameFaction(FactionAPI faction, String newName)
	{
		faction.setDisplayNameOverride(newName);
		char initialLetter = (faction.getDisplayName().toLowerCase()).charAt(0);
		if (initialLetter == 'a' || initialLetter == 'e' || initialLetter == 'i' || initialLetter == 'o' || initialLetter == 'u')
		{
			faction.setPersonNamePrefixAOrAnOverride("an");
		}
		else
		{
			faction.setPersonNamePrefixAOrAnOverride("a");
		}
	}
	
	/**
	 * Random chance (chance = EXCHANGE_CHANCE) to exchange a faction's equipment with a nearby minor faction
	 * @param minorFaction
	 * @param nearbyMarkets - list of the nearby markets
	 * @param hulls - if true, exchange hulls with minor faction
	 * @param weapons - if true, exchange weapons with minor faction
	 * @param fighters - if true, exchange fighters with minor faction
	 */
	public static void exchangeTech(FactionAPI minorFaction, List<MarketAPI> nearbyMarkets, boolean hulls, boolean weapons, boolean fighters)
	{
		if (nearbyMarkets == null || nearbyMarkets.size() == 0) return;
		
		if (hulls)
		{
			Set<String> nearbyHulls = new HashSet<String>();
			
			for (MarketAPI m : nearbyMarkets)
			{
				nearbyHulls.addAll(m.getFaction().getKnownShips());
			}
			
			for (Iterator<String> i = nearbyHulls.iterator(); i.hasNext();)
			{
				String s = i.next();
				if (minorFaction.knowsShip(s)) i.remove();
			}
			
			int numShipsToAdd = StarSystemGenerator.random.nextInt(MAX_NUMBER_OF_EXCHANGE_PER_CATEGORY);
			
			if (numShipsToAdd > nearbyHulls.size())
			{
				for (String s : nearbyHulls)
				{
					FleetMemberAPI ship = Global.getFactory().createFleetMember(FleetMemberType.SHIP, s);
					
					if (!ship.isCruiser() && !ship.isCapital() && !ship.isCarrier())
					{
						minorFaction.addKnownShip(s, true);
						log.info("Adding ship " + s + " to minor faction " + minorFaction.getDisplayName());
						
						nearbyHulls.remove(s);
					}
				}
			}
			else
			{
				for (int i = 0; i < numShipsToAdd; i++)
				{
					String selected = (String) (nearbyHulls.toArray())[StarSystemGenerator.random.nextInt(nearbyHulls.size())];
					
					FleetMemberAPI ship = Global.getFactory().createFleetMember(FleetMemberType.SHIP, selected);
					
					if (!ship.isCruiser() && !ship.isCapital() && !ship.isCarrier())
					{
						minorFaction.addKnownShip(selected, true);
						log.info("Adding ship " + selected + " to minor faction " + minorFaction.getDisplayName());
						
						nearbyHulls.remove(selected);
					}
					else i--;
				}
			}
			
		}
		
		if (weapons)
		{
			Set<String> nearbyWeapons = new HashSet<String>();
			
			for (MarketAPI m : nearbyMarkets)
			{
				nearbyWeapons.addAll(m.getFaction().getKnownWeapons());
			}
			
			for (Iterator<String> i = nearbyWeapons.iterator(); i.hasNext();)
			{
				String s = i.next();
				if (minorFaction.knowsWeapon(s)) i.remove();
			}
			
			int numWeaponsToAdd = StarSystemGenerator.random.nextInt(MAX_NUMBER_OF_EXCHANGE_PER_CATEGORY);
			
			if (numWeaponsToAdd > nearbyWeapons.size())
			{
				for (String s : nearbyWeapons)
				{
					minorFaction.addKnownWeapon(s, true);
					log.info("Adding weapon " + s + " to minor faction " + minorFaction.getDisplayName());
				}
			}
			else
			{
				for (int i = 0; i < numWeaponsToAdd; i++)
				{
					String selected = (String) (nearbyWeapons.toArray())[StarSystemGenerator.random.nextInt(nearbyWeapons.size())];
					minorFaction.addKnownWeapon(selected, true);
					log.info("Adding weapon " + selected + " to minor faction " + minorFaction.getDisplayName());
					
					nearbyWeapons.remove(selected);
				}
			}
			
		}
		
		if (fighters)
		{
			Set<String> nearbyFighters = new HashSet<String>();
			
			for (MarketAPI m : nearbyMarkets)
			{
				nearbyFighters.addAll(m.getFaction().getKnownFighters());
			}
			
			for (Iterator<String> i = nearbyFighters.iterator(); i.hasNext();)
			{
				String s = i.next();
				if (minorFaction.knowsFighter(s)) i.remove();
			}
			
			int numFightersToAdd = StarSystemGenerator.random.nextInt(MAX_NUMBER_OF_EXCHANGE_PER_CATEGORY);
			
			if (numFightersToAdd > nearbyFighters.size())
			{
				for (String s : nearbyFighters)
				{
					minorFaction.addKnownFighter(s, true);
					log.info("Adding fighter " + s + " to minor faction " + minorFaction.getDisplayName());
				}
			}
			else
			{
				for (int i = 0; i < numFightersToAdd; i++)
				{
					String selected = (String) (nearbyFighters.toArray())[StarSystemGenerator.random.nextInt(nearbyFighters.size())];
					minorFaction.addKnownFighter(selected, true);
					log.info("Adding fighter " + selected + " to minor faction " + minorFaction.getDisplayName());
					
					nearbyFighters.remove(selected);
				}
			}
			
		}
	}
	
	public static void illegalCommodityRandomization(FactionAPI minorFaction, List<MarketAPI> nearbyMarkets)
	{
		Set<String> illegal = minorFaction.getIllegalCommodities();
		Set<String> nearbyIllegal = new HashSet<String>();
		
		for (MarketAPI market : nearbyMarkets)
		{
			for (String ill : market.getFaction().getIllegalCommodities())
			{
				nearbyIllegal.add(ill);
			}
		}
		
		// xor the two Sets to get difference
		Set<String> illegalDifference = new HashSet<String>();
		for (String s : illegal)
		{
			if (!nearbyIllegal.contains(s)) illegalDifference.add(s);
		}
		for (String s : nearbyIllegal)
		{
			if (!illegal.contains(s)) illegalDifference.add(s);
		}
		
		// modify faction illegal commodities
		for (String s : illegalDifference)
		{
			float roll = StarSystemGenerator.random.nextFloat();
			
			// make commodity legal
			if (roll < ILLEGAL_EXCHANGE_CHANCE && illegal.contains(s))
			{
				minorFaction.makeCommodityLegal(s);
				log.info("Commodity " + s + " is now legal in " + minorFaction.getDisplayName());
			}
			// make commodity illegal
			else if (roll < ILLEGAL_EXCHANGE_CHANCE && !illegal.contains(s))
			{
				minorFaction.makeCommodityIllegal(s);
				log.info("Commodity " + s + " is now illegal in " + minorFaction.getDisplayName());
			}
		}
	}
	
	public static void randomizeFlag(FactionAPI minorFaction, boolean isPirate)
	{
		String[] flagList = isPirate ? SSGConfig.pirateFlagList : SSGConfig.independentFlagList;
		
		if (flagList.length == 0) return;
		
		Set<String> availableFlags = new HashSet<String>();
		for (String s : flagList)
		{
			if (!SSGConfig.usedFlags.contains(s)) availableFlags.add(s);
		}
		
		if (availableFlags.size() == 0)
		{
			log.info("No extra flags available for " + (isPirate ? "pirate " : "independent ") + minorFaction.getDisplayName());
			return;
		}
		
		String flagNumber = (String) (availableFlags.toArray())[StarSystemGenerator.random.nextInt(availableFlags.size())];

		log.info("Setting " + minorFaction.getDisplayName() + " flag/crest to " + flagNumber + "_{flag,crest}.png");
		minorFaction.setFactionCrestOverride("graphics/factions/" + flagNumber + "_crest.png");
		minorFaction.setFactionLogoOverride("graphics/factions/" + flagNumber + "_flag.png");
		
		SSGConfig.usedFlags.add(flagNumber);
	}
	
	public static void randomizeMorality(FactionAPI faction)
	{
		return;
		/*
		final float GOOD_CHANCE = 0.5f;
		final float AMORAL_CHANCE = 0.05f;
		
		final float roll = StarSystemGenerator.random.nextFloat();
		
		if (roll >= AMORAL_CHANCE) ExerelinConfig.getExerelinFactionConfig(faction.getId()).morality = Morality.AMORAL;
		else if (roll >= GOOD_CHANCE + AMORAL_CHANCE) ExerelinConfig.getExerelinFactionConfig(faction.getId()).morality = Morality.GOOD;
		*/
	}
}
