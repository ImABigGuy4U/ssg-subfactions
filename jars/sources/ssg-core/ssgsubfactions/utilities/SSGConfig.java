package ssgsubfactions.utilities;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.fs.starfarer.api.Global;

import exerelin.utilities.ExerelinUtils;

public class SSGConfig {
	
	public static final String CONFIG_PATH = "ssg_config.json";
	
	public static Logger log = Global.getLogger(SSGConfig.class);
	
	// minor independent/pirate factions
	public static String[] minorFactionList = new String[]{};
	public static String[] minorPiratesList = new String[]{};
	public static String[] independentFlagList = new String[]{};
	public static String[] pirateFlagList = new String[]{};
	public static Set<String> usedFlags = new HashSet<String>();
	public static boolean showMinorIndependentsInIntel = true;
	public static boolean showMinorPiratesInIntel = false;
	public static boolean enableMinorPirates = false;
	public static boolean limitNumberIndependents = false;
	public static int maxNumberOfStartingIndependents = 999;
	public static boolean limitNumberPirates = false;
	public static int maxNumberOfStartingPirates = 999;
	
	public static void loadSettings()
    {
        try
        {
        	log.info("Loading ssg_config settings");

            JSONObject settings = Global.getSettings().getMergedJSONForMod(CONFIG_PATH, "ssg_minimods");
            
            minorFactionList = ExerelinUtils.JSONArrayToStringArray(settings.optJSONArray("minor_independents"));
            log.info("Number of minor factions: " + minorFactionList.length);
            
            minorPiratesList = ExerelinUtils.JSONArrayToStringArray(settings.optJSONArray("minor_pirates"));
            log.info("Number of minor pirate factions: " + minorPiratesList.length);
            
            independentFlagList = ExerelinUtils.JSONArrayToStringArray(settings.optJSONArray("independent_flag_list"));
            log.info("Number of independent faction flags: " + independentFlagList.length);
            
            showMinorIndependentsInIntel = settings.optBoolean("show_minor_independents_in_intel", showMinorIndependentsInIntel);
            
            showMinorPiratesInIntel = settings.optBoolean("show_minor_pirates_in_intel", showMinorPiratesInIntel);
            
            enableMinorPirates = settings.optBoolean("enable_minor_pirates", enableMinorPirates);
            
            limitNumberIndependents = settings.optBoolean("limit_starting_number_independents", limitNumberIndependents);
            
            maxNumberOfStartingIndependents = settings.optInt("max_number_starting_independents", maxNumberOfStartingIndependents);
            
            limitNumberPirates = settings.optBoolean("limit_starting_number_pirates", limitNumberPirates);
            
            maxNumberOfStartingPirates = settings.optInt("max_number_starting_pirates", maxNumberOfStartingPirates);
        }
        catch(Exception e)
        {
            throw new RuntimeException("Failed to load config: " + e.getMessage(), e);
        }
    }
	
	static {
		loadSettings();
	}
}
