package ssgsubfactions.campaign.econ.industry;

import com.fs.starfarer.api.impl.campaign.econ.impl.BaseIndustry;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Industries;

import ssgsubfactions.utilities.SSGUtilsMinorFactions;

public class SalvageOperations extends BaseIndustry {

	@Override
	public void apply()
	{
		super.apply(true);
		//int size = market.getSize();
		
		//supply(Commodities.METALS, size - 4);
		supply(Commodities.SHIPS, 2);
		
		//int machine_cost = size - 2;
		
		//if (size >= 5) machine_cost--;
		
		demand(Commodities.CREW, 1);
		demand(Commodities.HEAVY_MACHINERY, 1);
		
		if (!isFunctional()) {
			supply.clear();
			unapply();
		}
		else if (market.hasIndustry(Industries.HEAVYINDUSTRY) || market.hasIndustry(Industries.ORBITALWORKS))
		{
			// remove this structure, as it has been superceded
			supply.clear();
			demand.clear();
			unapply();
			//market.removeIndustry("salvageoperations", null, false);
		}
	}

	@Override
	public boolean isAvailableToBuild()
	{
		boolean validFaction = (market.getFactionId().equals(Factions.INDEPENDENT) || SSGUtilsMinorFactions.isMinorIndependent(market.getFactionId()) || SSGUtilsMinorFactions.isMinorPirate(market.getFactionId()) || market.getFactionId().equals(Factions.LUDDIC_PATH) || market.getFactionId().equals(Factions.PIRATES));
		return (validFaction && !market.hasIndustry(Industries.ORBITALWORKS) && !market.hasIndustry(Industries.HEAVYINDUSTRY));
	}
	
	@Override
	public boolean showWhenUnavailable()
	{
		return false;
	}
}
