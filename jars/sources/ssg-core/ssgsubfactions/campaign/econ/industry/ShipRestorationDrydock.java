package ssgsubfactions.campaign.econ.industry;

import com.fs.starfarer.api.impl.campaign.econ.impl.BaseIndustry;
import com.fs.starfarer.api.impl.campaign.econ.impl.ShipQuality;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Industries;
import com.fs.starfarer.api.impl.campaign.ids.Stats;

import ssgsubfactions.utilities.SSGUtilsMinorFactions;

public class ShipRestorationDrydock extends BaseIndustry {
	
	public static float SHIP_QUALITY_BONUS = 0.15f; // it's hard to have no cross-faction ship imports when you are a one-planet faction without a heavy industry

	@Override
	public void apply()
	{
		super.apply(true);
		
		float qualityBonus = SHIP_QUALITY_BONUS;
		if (ShipQuality.getInstance().getQualityData(market).quality.getFlatBonus("no_prod_penalty") != null)
		{ // does not have cross-faction imports penalty
			qualityBonus += 0.25f;
		}
		//int size = market.getSize();
		
		//demand(Commodities.HEAVY_MACHINERY, size - 2);
		demand(Commodities.HEAVY_MACHINERY, 1);
		
		market.getStats().getDynamic().getMod(Stats.FLEET_QUALITY_MOD).modifyFlat("shiprestorationdrydock", qualityBonus, "Ship Restoration Drydock");
		
		if (!isFunctional())
		{
			unapply();
		}
		else if (market.hasIndustry(Industries.HEAVYINDUSTRY) || market.hasIndustry(Industries.ORBITALWORKS))
		{
			// remove this structure, as it has been superceded
			unapply();
			demand.clear();
			//market.removeIndustry("shiprestorationdrydock", null, false);
		}
	}
	
	@Override
	public void unapply()
	{
		super.unapply();
		
		market.getStats().getDynamic().getMod(Stats.FLEET_QUALITY_MOD).unmodifyFlat("shiprestorationdrydock");
	}
	
	@Override
	public boolean isAvailableToBuild()
	{
		boolean validFaction = (market.getFactionId().equals(Factions.INDEPENDENT) || SSGUtilsMinorFactions.isMinorIndependent(market.getFactionId()) || SSGUtilsMinorFactions.isMinorPirate(market.getFactionId()) || market.getFactionId().equals(Factions.LUDDIC_PATH) || market.getFactionId().equals(Factions.PIRATES));
		return (validFaction && !market.hasIndustry(Industries.HEAVYINDUSTRY) && !market.hasIndustry(Industries.ORBITALWORKS));
	}

	@Override
	public boolean showWhenUnavailable()
	{
		return false;
	}
}
