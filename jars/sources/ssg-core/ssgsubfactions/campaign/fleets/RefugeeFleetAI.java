package ssgsubfactions.campaign.fleets;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.AsteroidAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.ai.FleetAssignmentDataAPI;
import com.fs.starfarer.api.util.Misc;
import exerelin.utilities.StringHelper;
import org.apache.log4j.Logger;
import org.lwjgl.util.vector.Vector2f;

import ssgsubfactions.campaign.fleets.RefugeeFleetManagerV2.RefugeeFleetData;

public class RefugeeFleetAI implements EveryFrameScript
{
	public static Logger log = Global.getLogger(RefugeeFleetAI.class);
	public static final float UPDATE_INTERVAL = 0.25f;
	
	protected final RefugeeFleetData data;
	
	protected float daysTotal = 0.0F;
	protected final CampaignFleetAPI fleet;
	protected boolean orderedReturn = false;
	protected boolean unloaded = false;
	protected boolean needReload = false;
	//protected EveryFrameScript broadcastScript;
	
	public RefugeeFleetAI(CampaignFleetAPI fleet, RefugeeFleetData data)
	{
		this.fleet = fleet;
		this.data = data;
		giveInitialAssignment();
	}
	
	protected SectorEntityToken getNearestSafePlanet()
	{
		float closestDistSq = 99999999;
		SectorEntityToken closest = null;
		
		
		return closest;
	}
	
	protected Object readResolve()
	{
		// our asteroid won't be reloaded in star system on its own, we'll put it back in advance()
		if (data.target != null && data.target instanceof AsteroidAPI)
		{
			needReload = true;
		}
		return this;
	}
	
	float interval = 0;
	
	@Override
	public void advance(float amount)
	{
		float days = Global.getSector().getClock().convertToDays(amount);
		this.daysTotal += days;
		if (this.daysTotal > 150.0F)
		{
			giveStandDownOrders();
			return;
		}
		
		interval += days;
		if (interval >= UPDATE_INTERVAL) interval -= UPDATE_INTERVAL;
		else return;
		
		// reset asteroid
		if (needReload)
		{
			needReload = false;
			fleet.getContainingLocation().addEntity(data.target);
		}
		
		FleetAssignmentDataAPI assignment = this.fleet.getAI().getCurrentAssignment();
		/*if (assignment != null)
		{
			CargoAPI cargo = fleet.getCargo();
			if (cargo.getSpaceUsed() / cargo.getMaxCapacity() > 0.9f)
			{
				giveStandDownOrders();
			}
			
			if (orderedReturn)
			{
				// unload cargo if appropriate
				if(!unloaded && assignment.getAssignment() == FleetAssignment.ORBIT_PASSIVE && data.source.getContainingLocation() == data.fleet.getContainingLocation()
						&& MathUtils.getDistance(data.source, data.fleet) < 600f)
				{
					List<CargoStackAPI> cargoStacks = cargo.getStacksCopy();
					for (CargoStackAPI stack : cargoStacks)
					{
						if (stack.isCommodityStack() && !stack.isSupplyStack())
						{
							ExerelinUtilsCargo.addCommodityStockpile(data.sourceMarket, stack.getCommodityId(), stack.getSize());
							cargo.addCommodity(stack.getCommodityId(), -stack.getSize());
						}
					}
					cargo.removeEmptyStacks();
					unloaded = true;
				}
				return;
			}
		}*/
		
		if (assignment != null)
		{
			return;
		}
		
		// no orders, head to mining target
		SectorEntityToken target = data.target;
		LocationAPI loc = target.getContainingLocation();
		String locName = "the " + loc.getName();
		String tgtName = target.getName();
		if (loc != fleet.getContainingLocation()) {
			LocationAPI hyper = Global.getSector().getHyperspace();
			Vector2f dest = Misc.getPointAtRadius(loc.getLocation(), 1500.0F);
			SectorEntityToken token = hyper.createToken(dest.x, dest.y);
			fleet.addAssignment(FleetAssignment.GO_TO_LOCATION, target, 1000.0F, StringHelper.getFleetAssignmentString("travellingToStarSystem", tgtName));
		}
		else {
			//fleet.addAssignment(FleetAssignment.GO_TO_LOCATION, target, 1000.0F, StringHelper.getFleetAssignmentString("travellingTo", tgtName));
			giveStandDownOrders();
		}
		//fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, target, 30, StringHelper.getFleetAssignmentString("fleeing", tgtName));
	}
	
	@Override
	public boolean isDone()
	{
		return !fleet.isAlive();
	}
	
	@Override
	public boolean runWhilePaused()
	{
		return false;
	}
	
	protected void giveInitialAssignment()
	{
		if (data.noWait) return;
		float daysToOrbit = 1.0F;//ExerelinUtilsFleet.getDaysToOrbit(fleet);
		//fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, this.data.source, daysToOrbit, StringHelper.getFleetAssignmentString("preparingFor", data.source.getName(), "missionRefugee"));
		//fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, this.data.source, daysToOrbit, StringHelper.getFleetAssignmentString("preparingFor", data.source.getName(), "travellingTo"));
		fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, this.data.source, daysToOrbit, StringHelper.getFleetAssignmentString("preparingFor", data.source.getName(), "missionMining"));
	}
	
	protected void debugLocal(String str)
	{
		if (fleet.getContainingLocation() != Global.getSector().getPlayerFleet().getContainingLocation())
			return;
		Global.getSector().getCampaignUI().addMessage(str);
	}
	
	protected void giveStandDownOrders()
	{
		if (!orderedReturn)
		{
			orderedReturn = true;
			fleet.clearAssignments();
			
			SectorEntityToken target = data.target;
			fleet.addAssignment(FleetAssignment.GO_TO_LOCATION, target, 1000.0F, StringHelper.getFleetAssignmentString("travellingTo", target.getName()));			
			fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, target, 1000.0F);
		}
	}
}

