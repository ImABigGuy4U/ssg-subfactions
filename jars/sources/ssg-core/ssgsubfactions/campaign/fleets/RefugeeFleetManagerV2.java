package ssgsubfactions.campaign.fleets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.OrbitAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.fleets.DisposableFleetManager;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Industries;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.util.WeightedRandomPicker;

//import exerelin.campaign.MiningHelperLegacy;
import exerelin.utilities.ExerelinConfig;
import exerelin.utilities.ExerelinFactionConfig;
import exerelin.utilities.ExerelinUtilsFaction;
import exerelin.utilities.ExerelinUtilsFleet;
import exerelin.utilities.ExerelinUtilsMarket;
import exerelin.utilities.StringHelper;

import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.MathUtils;

public class RefugeeFleetManagerV2 extends DisposableFleetManager
{
	public static final String MANAGER_MAP_KEY = "exerelin_RefugeeFleetManager";
	
	protected static final float POINT_INCREMENT_PERIOD = 1;
	
	protected static final float DECIV_SPAWN_REFUGEE_FLEET_CHANCE = 0.5f;
	
	public static Logger log = Global.getLogger(RefugeeFleetManagerV2.class);
	
	protected float timer = 0;
	protected float daysElapsed = 0;
	protected Random random = new Random();
	
	public CampaignFleetAPI spawnRefugeeFleet(MarketAPI origin)
	{
		return null;
		//if () return null; // not in decivilization so don't spawn
	
		/*log.info("Trying refugee fleet for market " + origin.getName());
		SectorEntityToken target = null;
		
		FactionAPI faction = origin.getFaction();
		int marketSize = origin.getSize();
		int maxFP = (int)(Math.pow(marketSize, 1.5f) * 5);
		
		WeightedRandomPicker<SectorEntityToken> targetPicker = new WeightedRandomPicker<>();
		//List<SectorEntityToken> planets = origin.getContainingLocation().getEntitiesWithTag("planet");
		
		boolean sameFactionTargetFound = false;
		//log.info("Number of markets for faction " + faction.getId() + ": " + ExerelinUtilsFaction.getFactionMarkets(faction.getId()).size());
		for (MarketAPI market : ExerelinUtilsFaction.getFactionMarkets(faction.getId()))
		{
			if (SSGUtilsMarket.chanceToDeciv(market) || market.equals(origin))	// don't flee to a decivilizing system
				continue;
			if (random.nextFloat() >= DECIV_SPAWN_REFUGEE_FLEET_CHANCE)
				continue;
			float dist = ExerelinUtilsMarket.getHyperspaceDistance(origin, market);	// prefer closer systems
			float stab = market.getStabilityValue();			// prefer more stable systems
			targetPicker.add(market.getPrimaryEntity(), (stab / (dist + 0.00001F)));
			sameFactionTargetFound = true;
		}
		
		if (!sameFactionTargetFound)	// if no in-faction targets found, search for nearby allied or neutral systems
		{
			log.info("No suitable in-faction targets found for refugee fleet from " + origin.getName());
			for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy())
			{
				if (market.getFaction().isHostileTo(faction))
					continue;
				if (!SSGUtilsMarket.chanceToDeciv(market))
					continue;
				if (random.nextFloat() >= DECIV_SPAWN_REFUGEE_FLEET_CHANCE)
					continue;
				float dist = ExerelinUtilsMarket.getHyperspaceDistance(origin, market);	// prefer closer systems
				float stab = market.getStabilityValue();			// prefer more stable systems
				targetPicker.add(market.getPrimaryEntity(), (stab / (dist + 0.00001F)));
			}
		}
		
		if (targetPicker.isEmpty()) {
			log.info("No valid target for refugee fleet");
			return null;
		}
		else target = targetPicker.pick();
		if (target == null)
		{
			log.info("Refugee fleet target is null; picker size " + targetPicker.getItems().size());
			return null;
		}
		log.info("Fleeing to market " + target.getFullName());
		
		String name = StringHelper.getString("ssgminimods_fleets", "refugeeFleetName");
		String factionId = origin.getFactionId();
		
		//log.info("Trying to create mining fleet of size " + maxFP + ", target " + target.getName());
		FleetParamsV3 params = new FleetParamsV3(origin, "ssgminimodsRefugeeFleet", 
				maxFP*0.05f, // combat
				maxFP*0.15f, // freighters
				0,		// tankers
				0,		// personnel transports
				maxFP*0.75f,		// liners
				maxFP*0.05f,	// utility
				-0.25f);	// quality mod
		
		//CampaignFleetAPI fleet = FleetFactory.createGenericFleet(origin.getFactionId(), name, qf, maxFP/3);
		CampaignFleetAPI fleet = ExerelinUtilsFleet.customCreateFleet(faction, params);
		if (fleet == null)
			return null;
		
		fleet.setName(name);
		fleet.setAIMode(true);
		fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_TRADE_FLEET, true);
		
		// take goods with us
		float foodToTake = Math.min(marketSize * 125f, origin.getCommodityData(Commodities.FOOD).getStockpile());
		float populationToTake = Math.min(marketSize * 125f, origin.getCommodityData(Commodities.CREW).getStockpile());
		fleet.getCargo().addCommodity(Commodities.FOOD, foodToTake);
		fleet.getCargo().addCommodity(Commodities.CREW, populationToTake);
		origin.getCommodityData(Commodities.FOOD).removeFromStockpile(foodToTake);
		origin.getCommodityData(Commodities.CREW).removeFromStockpile(populationToTake);
		
		SectorEntityToken entity = origin.getPrimaryEntity();
		entity.getContainingLocation().addEntity(fleet);
		CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();
		
		boolean noWait = true;
		if (playerFleet != null && playerFleet.getContainingLocation() == entity.getContainingLocation())
			noWait = !MathUtils.isWithinRange(target, playerFleet, playerFleet.getMaxSensorRangeToDetect(fleet) + getInSystemCullRange());
		
		if (!noWait) {
			fleet.setLocation(entity.getLocation().x, entity.getLocation().y);
		}
		else {
			fleet.setLocation(target.getLocation().x, target.getLocation().y);
		}
		
		RefugeeFleetData data = new RefugeeFleetData(fleet);
		data.sourceMarket = origin;
		data.source = origin.getPrimaryEntity();
		data.target = target;
		data.noWait = noWait;
		
		// partially fill cargo (as if we're already been mining for some time)
		if (noWait) {
			//MiningReport report = MiningHelperLegacy.getMiningReport(fleet, target, miningStrength);
			float mult = MathUtils.getRandomNumberInRange(0f, 0.75f);
			if (mult > 0.1) {
				int cargoSpace = (int)(fleet.getCargo().getSpaceLeft() * mult);
			
				float totalAmount = 0;
			}
		}
		
		RefugeeFleetAI ai = new RefugeeFleetAI(fleet, data);
		fleet.addScript(ai);
		log.info("\tSpawned " + fleet.getNameWithFaction() + " of size " + maxFP);
		
		return fleet;*/
	}
	
	/*public CampaignFleetAPI spawnRefugeeFleet(MarketAPI origin, MarketAPI target)
	{
		CampaignFleetAPI fleetTmp = spawnRefugeeFleet(origin);
		RefugeeFleetData fleetDataTmp = (RefugeeFleetData) fleetTmp.getFleetData();
		fleetDataTmp.target = target.getPrimaryEntity();
		return fleetTmp;
	}*/
	
	@Override
	protected String getActionInsideText(StarSystemAPI system) {
		return "fleeing";
	}
	
	@Override
	public void advance(float amount)
	{
		super.advance(amount);
		float days = Global.getSector().getClock().convertToDays(amount);
		
		timer += days;
		if (timer < POINT_INCREMENT_PERIOD) {
			return;
		}
		timer -= POINT_INCREMENT_PERIOD;
	}
	
	public static RefugeeFleetManagerV2 create()
	{
		Map<String, Object> data = Global.getSector().getPersistentData();
		RefugeeFleetManagerV2 manager = (RefugeeFleetManagerV2)data.get(MANAGER_MAP_KEY);
		if (manager != null)
			return manager;
		
		manager = new RefugeeFleetManagerV2();
		data.put(MANAGER_MAP_KEY, manager);
		return manager;
	}
	
	public static RefugeeFleetManagerV2 getManager()
	{
		Map<String, Object> data = Global.getSector().getPersistentData();
		return (RefugeeFleetManagerV2)data.get(MANAGER_MAP_KEY);
	}

	@Override
	protected int getDesiredNumFleetsForSpawnLocation() {
		StarSystemAPI system = currSpawnLoc;
		return Global.getSector().getEconomy().getMarkets(system).size();
	}

	@Override
	protected CampaignFleetAPI spawnFleetImpl() {
		return null;
		/*StarSystemAPI system = currSpawnLoc;
		if (system == null) return null;
		
		WeightedRandomPicker<MarketAPI> picker = new WeightedRandomPicker<>();
		for (MarketAPI market : Global.getSector().getEconomy().getMarkets(system))
		{
			if (false)//SSGUtilsMarket.chanceToDeciv(market))
				continue;
			if (random.nextFloat() >= DECIV_SPAWN_REFUGEE_FLEET_CHANCE)
				continue;
			//float dist = ExerelinUtilsMarket.getHyperspaceDistance(, market);	// prefer closer systems
			//float stab = market.getStabilityValue();			// prefer more stable systems
			//targetPicker.add(market.getPrimaryEntity(), (stab / (dist + 0.00001F)));
			picker.add(market, market.getSize());
		}
		
		if (picker.isEmpty()) return null;
		MarketAPI market = picker.pick();
		
		CampaignFleetAPI fleet = spawnRefugeeFleet(market);
		
		return fleet;*/
	}

	@Override
	protected String getSpawnId() {
		return "ssgminimodsRefugeeFleet";
	}
	
	@Override
	protected boolean isOkToDespawnAssumingNotPlayerVisible(CampaignFleetAPI fleet) {
		if (currSpawnLoc == null) return true;
		String system = fleet.getMemoryWithoutUpdate().getString(KEY_SYSTEM);
		if (system == null || !system.equals(currSpawnLoc.getName())) return true;
		return false;
	}
	
	@Override
	protected float getExpireDaysPerFleet() {
		return 30f;
	}
	
	@Override
	public float getSpawnRateMult() {
		return super.getSpawnRateMult() * 1f;
	}
	
	public static class RefugeeFleetData
	{
		public CampaignFleetAPI fleet;
		public SectorEntityToken source;
		public SectorEntityToken target;
		public MarketAPI sourceMarket;
		public boolean noWait = false;
	
		public RefugeeFleetData(CampaignFleetAPI fleet)
		{
			this.fleet = fleet;
		}
	}
}