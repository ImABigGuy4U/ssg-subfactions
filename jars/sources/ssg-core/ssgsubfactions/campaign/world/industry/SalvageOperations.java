package ssgsubfactions.campaign.world.industry;

import org.apache.log4j.Logger;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Industries;

import exerelin.world.ExerelinProcGen.ProcGenEntity;
import exerelin.world.industry.IndustryClassGen;
import ssgsubfactions.utilities.SSGUtilsMinorFactions;

public class SalvageOperations extends IndustryClassGen {
	
	public static Logger log = Global.getLogger(SalvageOperations.class);
	
	public SalvageOperations()
	{
		super("salvageoperations");
	}

	@Override
	public float getWeight(ProcGenEntity entity)
	{
		MarketAPI market = entity.market;
		
		float weight = (60 + market.getSize() * 5) * 2;
				
		// good for high hazard worlds
		weight += market.getHazardValue() * 2;
		
		// prefer not to be on same planet as fuel production
		if (market.hasIndustry(Industries.FUELPROD))
			weight -= 20;
		// or light industry
		if (market.hasIndustry(Industries.LIGHTINDUSTRY))
			weight -= 10;
		
		// disable if has heavy industry
		if (market.hasIndustry(Industries.HEAVYINDUSTRY) || market.hasIndustry(Industries.ORBITALWORKS))
			weight *= 0.0f;
		
		return weight;
	}
	
	@Override
	public boolean canApply(ProcGenEntity entity)
	{
		if (!super.canApply(entity)) return false;
		
		if (entity.market.hasIndustry(Industries.HEAVYINDUSTRY) || entity.market.hasIndustry(Industries.ORBITALWORKS)) return false;
		
		return (entity.market.getFactionId().equals(Factions.INDEPENDENT) || SSGUtilsMinorFactions.isMinorIndependent(entity.market.getFactionId()) || SSGUtilsMinorFactions.isMinorPirate(entity.market.getFactionId()) || entity.market.getFactionId().equals(Factions.LUDDIC_PATH) || entity.market.getFactionId().equals(Factions.PIRATES));
	}
}
