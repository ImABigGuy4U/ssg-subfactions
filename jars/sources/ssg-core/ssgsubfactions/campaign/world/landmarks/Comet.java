package ssgsubfactions.campaign.world.landmarks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CustomCampaignEntityAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator.StarSystemType;
import com.fs.starfarer.api.util.WeightedRandomPicker;

import exerelin.utilities.ExerelinUtilsAstro;
import exerelin.world.landmarks.BaseLandmarkDef;
import ssgsubfactions.campaign.orbits.EllipticalOrbitV2;

public class Comet extends BaseLandmarkDef {
	
	protected static final Set<String> DISALLOWED_STARS = new HashSet<>(Arrays.asList(new String[]{
		Conditions.DECIVILIZED, Conditions.RUINS_SCATTERED, Conditions.RUINS_EXTENSIVE, 
		Conditions.RUINS_WIDESPREAD, Conditions.RUINS_VAST, "US_virus"
	})); // TODO
	
	@Override
	public boolean isApplicableToEntity(SectorEntityToken entity)
	{
		PlanetAPI star = entity.getStarSystem().getStar();
		
		if (star == null)
			return false;
		
		if (star.getTypeId().equals("black_hole")) return false;
		
		if (!entity.getStarSystem().getType().equals(StarSystemType.SINGLE)) return false;
		
		return true;
	}
	
	@Override
	public List<SectorEntityToken> getRandomLocations() {
		WeightedRandomPicker<SectorEntityToken> picker = new WeightedRandomPicker<>(random);
		
		for (SectorEntityToken token : getEligibleLocations())
		{
			float weight = 0.0f;//TODO 1.0f;
			if (token.getStarSystem().getStar().getTypeId().equals("star_blue_supergiant"))
				weight *= 0.5f;
			picker.add(token, weight); // TODO maybe make certain star colors more likely to have comets
		}
		
		log.info("Comet picker size is " + picker.getItems().size());
		List<SectorEntityToken> results = new ArrayList<>();
		int count = getCount();
		for (int i=0; i<count; i++)
		{
			results.add(picker.pick());
		}
		
		return results;
	}
	
	@Override
	public int getCount() {
		int starSystemCount = Global.getSector().getStarSystems().size();
		return (int)Math.ceil(starSystemCount *= 1.1f);
	}
		
	@Override
	public void createAt(SectorEntityToken entity)
	{
		//TODO naming
		if (entity == null) return;
		CustomCampaignEntityAPI comet = entity.getContainingLocation().addCustomEntity(null, null, "comet", null);
		
		float orbitScale = (random.nextFloat() * 0.6f) + 0.8f;
		//float semiMajor = entity.getStarSystem().getStar().getRadius() * 20.0f * orbitScale;
		float semiMajor = entity.getStarSystem().getStar().getRadius() * 4.0f * orbitScale;
		float eccentricity = random.nextFloat() * 0.49f + 0.5f;
		
		float semiMinor = (float) (semiMajor * Math.sqrt(1 - Math.pow(eccentricity, 2.0f)));
		//float period = (float) ((2f * random.nextFloat() + 0.5f) * 360f / (1.1f - eccentricity));
		float period = (float) ((2f * random.nextFloat() + 0.5f) * 20f / (1.1f - eccentricity));
		EllipticalOrbitV2 orbit = new EllipticalOrbitV2(comet, entity.getStarSystem().getStar(), ExerelinUtilsAstro.getRandomAngle(),
				semiMinor, semiMajor, ExerelinUtilsAstro.getRandomAngle(), period, true);
		comet.setOrbit(orbit);
		//float orbitPeriod = ExerelinUtilsAstro.getOrbitalPeriod(entity, orbitRadius);
		//beacon.setCircularOrbitWithSpin(entity, ExerelinUtilsAstro.getRandomAngle(), orbitRadius, orbitPeriod, 20, 30);
		//beacon.getMemoryWithoutUpdate().set("$nex_plagueBeacon", true);
		comet.setDiscoverable(false); // TODO
		
		//Misc.setWarningBeaconGlowColor(beacon, Color.GREEN);
		//Misc.setWarningBeaconPingColor(beacon, Color.GREEN);
		
		log.info("Spawning comet around " + entity.getName() + ", " + entity.getContainingLocation().getName());
	}
	
	@Override
	protected boolean weighByMarketSize() {
		return false;
	}
	
	@Override
	protected boolean isProcgenOnly() {
		return true;
	}
}
