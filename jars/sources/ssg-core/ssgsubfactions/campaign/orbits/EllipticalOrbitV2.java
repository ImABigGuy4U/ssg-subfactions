package ssgsubfactions.campaign.orbits;

import org.apache.log4j.Logger;
import org.lazywizard.lazylib.FastTrig;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.campaign.orbits.EllipticalOrbit;
import org.lwjgl.util.vector.Vector2f;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.OrbitAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;

import exerelin.utilities.ExerelinUtilsAstro;

/**
 * Extends LazyLib EllipticalOrbit class
 * This implementation changes the constant orbital velocity to a non-constant orbital velocity based on current position
 */
public class EllipticalOrbitV2 extends EllipticalOrbit implements OrbitAPI
{
    protected final SectorEntityToken focus;
    protected final float orbitAngle, orbitWidth, orbitHeight, orbitSpeed,
            offsetSin, offsetCos, mu; // mu = gravitational parameter = G * M
    protected SectorEntityToken entity;
    protected float currentAngle;
    protected final boolean tidalLock;
    
    public static Logger log = Global.getLogger(EllipticalOrbitV2.class);

    /**
     * Creates an elliptical orbit around a focus object.
     *
     * @param focus        What to orbit around.
     * @param startAngle   The angle (in degrees) that the orbit will begin at.
     *                     0 degrees = right - this is not relative to
     *                     {@code orbitAngle}.
     * @param orbitWidth   The width of the ellipse that makes up the orbital
     *                     path (semi-minor axis).
     * @param orbitHeight  The height of the ellipse that makes up the orbital
     *                     path (semi-major axis).
     * @param orbitAngle   The angular offset of the ellipse that makes up the
     *                     orbital path.
     * @param daysPerOrbit How long it should take for us to make one orbit
     *                     around {@code focus}.
     *
     * @since 1.9
     */
    public EllipticalOrbitV2(SectorEntityToken entity, SectorEntityToken focus, float startAngle,
                           float orbitWidth, float orbitHeight, float orbitAngle, float daysPerOrbit, boolean tidalLock)
    {
    	super(focus, startAngle, orbitWidth, orbitHeight, orbitAngle, daysPerOrbit);
    	this.entity = entity;
        this.focus = focus;
        this.orbitWidth = orbitWidth;
        this.orbitHeight = orbitHeight;
        this.orbitAngle = orbitAngle;
        this.orbitSpeed = 360f / daysPerOrbit; // average
        if (daysPerOrbit <= 0.0f)
    	{
    		log.error("dayPerOrbit must be positive!");
    		this.mu = 0f;
    	}
    	else
    	{
    		this.mu = (float) (Math.pow(2 * Math.PI / daysPerOrbit, 2) * Math.pow(orbitHeight, 3));
    	}
        this.tidalLock = tidalLock;
        double rad = Math.toRadians(orbitAngle);
        offsetSin = (float) Math.sin(rad);
        offsetCos = (float) Math.cos(rad);
        setAngle(startAngle);
        calculateCurrentSpeed();
        //runcode StarSystemAPI system = (StarSystemAPI) Global.getSector().getCurrentLocation(); system.getEntityByName("Orbital Station").setOrbit(new org.lazywizard.lazylib.campaign.orbits.EllipticalOrbit(system.getEntityByName("Corvus II"), 90f, 400f, 900f, 45f, 1f));
    }

	/**
     * Returns the current angle along the orbital path of the orbiting entity.
     *
     * @return The angle of the current position along the elliptical path.
     *
     * @since 1.9
     */
    public float getAngle()
    {
        return currentAngle;
    }

    /**
     * Returns the width of the ellipsis used as a path.
     *
     * @return The width of the ellipsis in su.
     *
     * @since 1.9
     */
    public float getOrbitWidth()
    {
        return orbitWidth;
    }

    /**
     * Returns the height of the ellipsis used as a path.
     *
     * @return The height of the ellipsis in su.
     *
     * @since 1.9
     */
    public float getOrbitHeight()
    {
        return orbitHeight;
    }

    /**
     * Returns the offset angle of the ellipsis used as a path.
     *
     * @return The offset of the ellipsis used as a path, in degrees.
     *
     * @since 1.9
     */
    public float getOrbitAngle()
    {
        return orbitAngle;
    }

    /**
     * Explicitly sets where along our orbital path we should be.
     *
     * @param angle The angle (in degrees) along the orbital path we should be
     *              moved to.
     *
     * @since 1.9
     */
    public void setAngle(float angle)
    {
        currentAngle = angle;

        // Don't bother with these calculations if there's no object to move!
        if (entity == null)
        {
            return;
        }

        angle = (float) Math.toRadians(angle);

        // Get point on unrotated ellipse around origin (0, 0)
        final float x = orbitWidth * (float) FastTrig.cos(angle);
        final float y = orbitHeight * (float) FastTrig.sin(angle);

        // Rotate point to match ellipses rotation and translate back to center
        entity.getLocation().set((x * offsetCos) - (y * offsetSin) + focus.getLocation().x,
                (x * offsetSin) + (y * offsetCos) + focus.getLocation().y);
    }
    
    private float calculateCurrentSpeed()
    {
    	return (float) (-(orbitWidth / Math.pow(ExerelinUtilsAstro.getCurrentOrbitRadius(this.getFocus(), this.entity), 2)) * Math.sqrt(mu / orbitHeight));
    	//return (float) (0.01f * Math.sqrt(mu * ((2 / ExerelinUtilsAstro.getCurrentOrbitRadius(this.getFocus(), this.entity)) - (1 / orbitHeight))));
    }

    /**
     * Returns the object we are orbiting.
     *
     * @return The {@link SectorEntityToken} we are orbiting around.
     */
    @Override
    public SectorEntityToken getFocus()
    {
        return focus;
    }

    /**
     * Called by Starsector itself - you can ignore this.
     */
    @Override
    public void setEntity(SectorEntityToken entity)
    {
        this.entity = entity;
        setAngle(currentAngle);
    }

    /**
     * Called by Starsector itself - you can ignore this.
     */
    @Override
    public void advance(float amount)
    {
        if (entity == null)
        {
            return;
        }

        // Advance rotation
        setAngle(MathUtils.clampAngle(currentAngle + (calculateCurrentSpeed()
                * Global.getSector().getClock().convertToDays(amount))));
        
        if (tidalLock)
        {
        	Vector2f eloc = entity.getLocation();
        	Vector2f floc = focus.getLocation();
        	
        	float angle = (float) Math.atan2(eloc.y - floc.y, eloc.x - floc.x);
        	
        	entity.setFacing(-angle); // i think TODO
        }
    }

    @Override
    public OrbitAPI makeCopy()
    {
        return new EllipticalOrbitV2(entity, focus, currentAngle, orbitWidth, orbitHeight, orbitAngle, orbitSpeed * 360f, tidalLock);
    }

    @Override
    public Vector2f computeCurrentLocation()
    {
        return entity.getLocation();
    }

    @Override
    public float getOrbitalPeriod()
    {
        return orbitSpeed * 360f;
    }
}
